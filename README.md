# Manganese ServerKit

## Installation

Add the Manganese Package Registry as a scoped registry to your Unity project's `manifest.json` file:

```
  "scopedRegistries": [
    {
      "name": "Manganese",
      "url": "http://unity.packages.mangane.se",
      "scopes": [
        "com.manganese"
      ]
    }
  ]
```

The Unity Package Manager should now find this package and give you the option to install it under "My Registries".

## Overview

This library lets you create HTTP servers with routing in Unity.  It includes:

- routing based on URI templates
- helpers for responding to requests with JSON, files, and binary data
- error handling and thread safety per request

## Usage

For information about routing, view the [UriTemplate](https://docs.microsoft.com/en-us/dotnet/api/system.uritemplate?view=netframework-4.0) and [UriTemplateMatch](https://docs.microsoft.com/en-us/dotnet/api/system.uritemplatematch?view=netframework-4.0) documentation.  ServerKit uses these under the hood to match routes and parse path parameters.

Starting an HTTP server on a specific port:

```cs
// Create a controller
var controller = new MyController();

// Set up the router
var router = new HttpRouter(new List<HttpRoute>() {
  // Example action
  new HttpRoute("GET", "/hello-world", new HttpRoute.RequestHandler(controller.HelloWorld))
});

// Start the server
var port = 80;
var driver = new HttpDriver(router, String.Format("http://*:{0}/", port));
driver.Start();
```

Receiving JSON in a request body:

```cs
public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    var requestBody = request.bodyAsJson; // as Dictionary<string, object>
  }
}
```

Deserializng the request body to an instance of a class:

```cs
public class MyRequestObject {
  public string myProperty { get; set; }
}

public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    var requstObject = request.bodyAs<MyRequestObject>();
  }
}
```

Reading the request body as a string:

```cs
public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    var data = request.body;
  }
}
```

Reading the request body as a byte array:

```cs
public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    var data = request.bytes;
  }
}
```

Sending a simple status code as a response:

```cs
public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    response.Status(404);
  }
}
```

Sending a JSON response:

```cs
public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    return response.Json(new Dictionary<string, object>() {
      { "success", true }
    });
  }
}
```

Sending a plain text response:

```cs
public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    var data = "Manganese ServerKit";

    return response.Text(data);
  }
}
```

Sending a binary response:

```cs
public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    var data = new byte[] {
      0x01,
      0x02,
      0x03
    };

    return response.Binary(data);
  }
}
```

Sending a file response:

```cs
public class MyController {
  public Task HelloWorld(HttpRequest request, HttpResponse response) {
    return response.File("/data/agreement.pdf");
  }
}
```

## Tips & Tricks

Pair this library with the Unity Async-Await library to access the main thread from a controller action:

```cs
public class MyController {
  public async Task HelloWorld(HttpRequest request, HttpResponse response) {
    // I'm in the request handler thread

    await new WaitForFixedUpdate();

    // Now I'm im the Unity thread and can instantiate game objects!
  }
}

```