using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.TestTools;

using Newtonsoft.Json;

using Manganese.ServerKit;

namespace Manganese.ServerKitTests {
  public class HttpRequestTests {
    [UnityTest]
    public IEnumerator BodyBytes() {
      using (var testContext = TestContext.Create()) {
        var data = new byte[] {
          0x01,
          0x02,
          0x03
        };

        using (var webRequest = UnityWebRequest.Put("/tests/requests/body/bytes", data)) {
          yield return webRequest.SendWebRequest();

          Assert.AreEqual(
            data,
            webRequest.downloadHandler.data
          );
        }
      }
    }

    [UnityTest]
    public IEnumerator BodyString() {
      using (var testContext = TestContext.Create()) {
        var data = "Manganese ServerKit";

        using (var webRequest = UnityWebRequest.Put("/tests/requests/body/string", data)) {
          yield return webRequest.SendWebRequest();

          Assert.AreEqual(
            data,
            webRequest.downloadHandler.text
          );
        }
      }
    }

    [UnityTest]
    public IEnumerator BodyJson() {
      using (var testContext = TestContext.Create()) {
        var data = new Dictionary<string, object>() {
          { "jsonWorks", true }
        };
        var serializedData = JsonConvert.SerializeObject(data);

        using (var webRequest = UnityWebRequest.Put("/tests/requests/body/string", serializedData)) {
          yield return webRequest.SendWebRequest();

          var deserializedData = JsonConvert.DeserializeObject<Dictionary<string, object>>(webRequest.downloadHandler.text);

          Assert.AreEqual(
            data,
            deserializedData
          );
        }
      }
    }

    [UnityTest]
    public IEnumerator PathParameters() {
      using (var testContext = TestContext.Create()) {
        var data = "25";
        var url = String.Format("/tests/requests/path-parameters/{0}", data);

        using (var webRequest = UnityWebRequest.Get(url)) {
          yield return webRequest.SendWebRequest();

          var response = JsonConvert.DeserializeObject<Dictionary<string, object>>(webRequest.downloadHandler.text);

          Assert.AreEqual(new Dictionary<string, object>() {
            { "ELEMENT", data }
          }, response);
        }
      }
    }

    [UnityTest]
    public IEnumerator QueryParameters() {
      using (var testContext = TestContext.Create()) {
        var parameterKey = "element";
        var parameterValue = "manganese";
        var parameter = String.Format("{0}={1}", parameterKey, parameterValue);
        var url = String.Format("/tests/requests/query-parameters?{0}", parameter);

        using (var webRequest = UnityWebRequest.Get(url)) {
          yield return webRequest.SendWebRequest();

          var response = JsonConvert.DeserializeObject<Dictionary<string, object>>(webRequest.downloadHandler.text);

          Assert.AreEqual(new Dictionary<string, object>() {
            { parameterKey, parameterValue }
          }, response);
        }
      }
    }
  }
}