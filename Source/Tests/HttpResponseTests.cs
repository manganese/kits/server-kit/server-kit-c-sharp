using System;
using System.Text;
using System.Collections;

using NUnit.Framework;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.TestTools;

using Manganese.ServerKit;

namespace Manganese.ServerKitTests {
  public class HttpResponseTests {
    [UnityTest]
    public IEnumerator JsonResponse() {
      using (var testContext = TestContext.Create()) {
        using (var webRequest = UnityWebRequest.Get("/tests/responses/json")) {
          yield return webRequest.SendWebRequest();

          Assert.AreEqual(
            @"{""jsonWorks"":true}",
            webRequest.downloadHandler.text
          );
        }
      }
    }

    [UnityTest]
    public IEnumerator TextResponse() {
      using (var testContext = TestContext.Create()) {
        using (var webRequest = UnityWebRequest.Get("/tests/responses/text")) {
          yield return webRequest.SendWebRequest();

          Assert.AreEqual(
            "Manganese ServerKit",
            webRequest.downloadHandler.data
          );
        }
      }
    }

    [UnityTest]
    public IEnumerator BinaryResponse() {
      using (var testContext = TestContext.Create()) {
        using (var webRequest = UnityWebRequest.Get("/tests/responses/binary")) {
          yield return webRequest.SendWebRequest();

          Assert.AreEqual(
            new byte[] {
              0x01,
              0x02,
              0x03
            },
            webRequest.downloadHandler.data
          );
        }
      }
    }

    [UnityTest]
    public IEnumerator FileResponse() {
      using (var testContext = TestContext.Create()) {
        using (var webRequest = UnityWebRequest.Get("/tests/responses/file")) {
          yield return webRequest.SendWebRequest();

          var fileData = ASCIIEncoding.Default.GetBytes(System.IO.File.ReadAllText("Assets/Package/Source/Tests/Support/Star.png"));

          Assert.AreEqual(
            fileData,
            webRequest.downloadHandler.data
          );
        }
      }
    }
  }
}