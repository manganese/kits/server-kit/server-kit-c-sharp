using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using NUnit.Framework;

using UnityEngine.TestTools;

using Manganese.ServerKit;

namespace Manganese.ServerKitTests {
  public class TestController {
    // Request tests
    public async Task TestRequestBodyBytes(HttpRequest request, HttpResponse response) {
      response.Binary(request.bytes);
    }

    public async Task TestRequestBodyString(HttpRequest request, HttpResponse response) {
      response.Text(request.body);
    }

    public async Task TestRequestBodyJson(HttpRequest request, HttpResponse response) {
      response.Binary(request.bytes);
    }

    public async Task TestRequestPathParameters(HttpRequest request, HttpResponse response) {
      response.Json(request.pathParameters);
    }

    public async Task TestRequestQueryParameters(HttpRequest request, HttpResponse response) {
      response.Json(request.queryParameters);
    }

    // Response tests
    public async Task TestResponseJson(HttpRequest request, HttpResponse response) {
      response.Json(new Dictionary<string, object>() {
        { "jsonWorks", true }
      });
    }

    public async Task TestResponseText(HttpRequest request, HttpResponse response) {
      var data = "Manganese ServerKit";

      response.Text(data);
    }

    public async Task TestResponseBinary(HttpRequest request, HttpResponse response) {
      var data = new byte[] {
        0x01,
        0x02,
        0x03
      };

      response.Binary(data);
    }

    public async Task TestResponseFile(HttpRequest request, HttpResponse response) {
      response.File("Assets/Package/Source/Tests/Support/Star.png");
    }
  }
}