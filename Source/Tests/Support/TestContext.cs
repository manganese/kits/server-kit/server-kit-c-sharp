using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Manganese.ServerKit;

namespace Manganese.ServerKitTests {
  public class TestContext : IDisposable {
    public static TestContext Create() {
      var context = new TestContext();

      return context;
    }

    private HttpDriver driver;

    public void Dispose() {
      this.driver.Dispose();
    }

    public TestContext() {
      var controller = new TestController();
      var router = new HttpRouter(new List<HttpRoute>() {
        // Request tests
        new HttpRoute("PUT", "/tests/requests/body/bytes", new HttpRoute.RequestHandler(controller.TestRequestBodyBytes)),
        new HttpRoute("PUT", "/tests/requests/body/string", new HttpRoute.RequestHandler(controller.TestRequestBodyString)),
        new HttpRoute("PUT", "/tests/requests/body/json", new HttpRoute.RequestHandler(controller.TestRequestBodyJson)),
        new HttpRoute("GET", "/tests/requests/path-parameters/{element}", new HttpRoute.RequestHandler(controller.TestRequestPathParameters)),
        new HttpRoute("GET", "/tests/requests/query-parameters", new HttpRoute.RequestHandler(controller.TestRequestQueryParameters)),

        // Response tests
        new HttpRoute("GET", "/tests/responses/json", new HttpRoute.RequestHandler(controller.TestResponseJson)),
        new HttpRoute("GET", "/tests/responses/text", new HttpRoute.RequestHandler(controller.TestResponseText)),
        new HttpRoute("GET", "/tests/responses/binary", new HttpRoute.RequestHandler(controller.TestResponseBinary)),
        new HttpRoute("GET", "/tests/responses/file", new HttpRoute.RequestHandler(controller.TestResponseFile))
      });
      this.driver = new HttpDriver(router);
      this.driver.Start();
    }
  }
}