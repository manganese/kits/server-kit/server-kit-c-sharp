using System;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;

using UnityEngine;

namespace Manganese.ServerKit {
  /**
   * @brief A HTTP route that specifies how a request should be handled if it matches.
   */
  public class HttpRoute {
    /// @name Constructor
    /// @{
    /**
     * @brief Create a new HTTP route.
     *
     * @param method  The HTTP method the request must use in order to match, or "*".
     * @param templateString  The pattern that the path of the URL must match.
     @ param requestHandler  The controller action to call for requests matching the route.
     *
     * Example:
     *
     * ```csharp
     * var route = new HttpRoute("GET", "/elements/{atomic_number}", new RequestHandler(elementsController.GetElement));
     * ```
     */
    public HttpRoute(string method, string templateString, RequestHandler requestHandler) {
      this.method = method;
      this.template = new UriTemplate(templateString);
      this.requestHandler = requestHandler;
    }
    /// @}

    /// @name Routing
    /// @{
    /**
     * @brief The HTTP method the request must use in order to match, or "*".
     */
    public readonly string method;

    /**
     * @brief The URI template that the path of the URL must match.
     */
    public readonly UriTemplate template;

    internal UriTemplateMatch MatchRequest(HttpListenerRequest httpRequest) {
      // Check method matches
      if (method != "*" && httpRequest.HttpMethod != method) {
        return null;
      }

      // Check URI template matches
      var url = httpRequest.Url;
      var prefix = new Uri(url.Scheme + "://" + url.Host + ":" + url.Port);

      return this.template.Match(prefix, url);
    }
    /// @}

    /// @name Request Handling
    /// @{
    /**
     * @brief The signature that a controller action must adhere to in order to handle requests.
     */
    public delegate Task RequestHandler(ServerKit.HttpRequest request, ServerKit.HttpResponse response);

    /**
     * @brief The controller action that will be called for matching requests.
     */
    public readonly RequestHandler requestHandler;

    internal async Task HandleRequest(ServerKit.HttpRequest request, ServerKit.HttpResponse response) {
      await this.requestHandler(request, response);
    }
    /// @}
  }
}