using System;
using System.ServiceModel;


namespace Manganese.ServerKit {
  internal class HttpRoutingResult {
    public readonly HttpRoute route;
    public readonly UriTemplateMatch templateMatch;

    internal HttpRoutingResult(HttpRoute route, UriTemplateMatch templateMatch) {
      this.route = route;
      this.templateMatch = templateMatch;
    }
  }
}