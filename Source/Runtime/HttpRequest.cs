using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;

using Newtonsoft.Json;


namespace Manganese.ServerKit {
  /**
   * @brief A HTTP request that is passed to a controller action.
   */
  public class HttpRequest {
    // Constructor
    internal HttpRequest(HttpRoutingResult routingResult, HttpListenerRequest httpRequest) {
      this.routingResult = routingResult;
      this.httpRequest = httpRequest;

      if (this.httpRequest.HasEntityBody) {
        var inputStream = this.httpRequest.InputStream;

        // Read body as byte array
        var contentLength = (int) this.httpRequest.ContentLength64;

        this.bytes = new byte[contentLength];

        inputStream.Read(this.bytes, 0, contentLength);

        // Read body as string
        this.body = Encoding.UTF8.GetString(this.bytes);
      }
    }

    // Utilities
    private static Dictionary<string, string> NameValueCollectionToDictionary(NameValueCollection collection) {
      var dictionary = new Dictionary<string, string>();

      foreach (var key in collection.AllKeys) {
        var value = collection[key];

        dictionary[key] = value;
      }

      return dictionary;
    }

    // Native HTTP request
    private readonly HttpListenerRequest httpRequest;

    // Routing
    private readonly HttpRoutingResult routingResult;

    /// @name Path Parameters
    /// @{
    /**
     * @brief The parameters matched in the URI template path.
     */
    public Dictionary<string, string> pathParameters {
      get {
        return NameValueCollectionToDictionary(this.routingResult.templateMatch.BoundVariables);
      }
    }
    /// @}

    /// @name Query String Parameters
    /// @{
    /**
     * @brief The query string parameters.
     */
    public Dictionary<string, string> queryParameters {
      get {
        return NameValueCollectionToDictionary(this.routingResult.templateMatch.QueryParameters);
      }
    }
    /// @}

    /// @name Body
    /// @{
    /**
     * @brief The body represented as a byte array.
     */
    public readonly byte[] bytes;

    /**
     * @brief The body represented as a string.
     *
     * @remark By default, the request body is read using UTF-8 encoding.
     */
    public readonly string body;

    /**
     * @brief Deserialize the JSON body to a given type.
     *
     * @remark The content type of the request must be set to "application/json".
     *
     * @tparam T  The type to deserialize as.
     */
    public T bodyAs<T>() {
      return JsonConvert.DeserializeObject<T>(this.body);
    }

    /**
     * @brief The body represented as an unstructured JSON object.
     */
    public Dictionary<string, object> bodyAsJson {
      get {
        return this.bodyAs<Dictionary<string, object>>();
      }
    }
    /// @}
  }
}