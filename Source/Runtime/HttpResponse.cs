using System.IO;
using System.Net;
using System.Text;

using Newtonsoft.Json;


namespace Manganese.ServerKit {
  /**
   * @brief A HTTP response that is passed to a controller action.
   */
  public class HttpResponse {
    // Constructor
    internal HttpResponse(HttpListenerResponse httpResponse) {
      this.httpResponse = httpResponse;
    }

    // Native HTTP response
    internal readonly HttpListenerResponse httpResponse;

    /**
     * @brief Set the response status code.
     *
     * @param statusCode  The status code.
     *
     * Example:
     *
     * ```csharp
     * response.Status(404);
     * ```
     */
    public void Status(int statusCode) {
      httpResponse.StatusCode = statusCode;
    }

    /**
     * @brief Set the content type of the response.
     *
     * @param mimeType  The mime type.
     *
     * Example:
     *
     * ```csharp
     * response.MimeType("application/xml");
     * ```
     */
    public void MimeType(string mimeType) {
      httpResponse.ContentType = mimeType;
    }

    /**
     * @brief Write binary data to the response.
     *
     * @param data  The data as a byte array.
     *
     * Example:
     *
     * ```csharp
     * var data = new byte[] {
     *   0x01,
     *   0x02,
     *   0x03
     * };
     *
     * response.Binary(data);
     * ```
     */
    public void Binary(byte[] data) {
      httpResponse.ContentLength64 = data.Length;
      httpResponse.OutputStream.Write(data, 0, data.Length);
    }

    /**
     * @brief Write plain text data to the response.
     *
     * @param data  The data as a string.
     *
     * Example:
     *
     * ```csharp
     * response.Text("Manganese ServerKit");
     * ```
     */
    public void Text(string data) {
      var serializedData = UTF8Encoding.Default.GetBytes(data);

      this.Binary(serializedData);
    }

    /**
     * @brief Write the contents of a file to the response.
     *
     * @param filePath  The path to the file.
     *
     * @remark If you use a relative path, it is relative to the root of the Unity project - not the Assets folder!
     *
     * Example:
     *
     * ```csharp
     * response.File("Assets/Resources/Star.png");
     * ```
     */
    public void File(string filePath) {
      var fileContents = UTF8Encoding.Default.GetBytes(System.IO.File.ReadAllText(filePath));

      this.Binary(fileContents);
    }

    /**
     * @brief Write the argument as JSON to the response.
     *
     * @param data  The object to serialize and write.
     *
     * @remark This will automatically set the content type to "application/json".
     * @remark Any object that Json.NET can serialize is valid to use.
     *
     * Example:
     *
     * ```csharp
     * response.Json(new Dictionary<string, object>() {
     *   { "success", true }
     * });
     * ```
     */
    public void Json(object data) {
      var text = JsonConvert.SerializeObject(data);

      this.MimeType("application/json");
      this.Text(text);
    }
  }
}