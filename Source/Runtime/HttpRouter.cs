using System;
using System.Net;
using System.ServiceModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Manganese.ServerKit {
  /**
   * @brief A HTTP router that tries requests against a set of routes until a match is found.
   */
  public class HttpRouter {
    /// @name Constructor
    /// @{
    /**
     * @brief Create a new HTTP router with.
     *
     * @param routes  The ordered set of routes to match each request against.
     *
     * Example:
     *
     * ```csharp
     * var router = new HttpRouter(new List<HttpRoute>() {
     *   new HttpRoute("GET", "/elements/{atomic_number}", new RequestHandler(elementsController.GetElement))
     * });
     * ```
     */
    public HttpRouter(IEnumerable<HttpRoute> routes) {
      this.routes = routes;
    }
    /// @}

    // Routing
    private IEnumerable<HttpRoute> routes;

    private HttpRoutingResult RouteRequest(HttpListenerRequest httpRequest) {
      foreach (var route in routes) {
        var templateMatch = route.MatchRequest(httpRequest);

        if (templateMatch == null) {
          continue;
        }

        return new HttpRoutingResult(route, templateMatch);
      }

      return null;
    }

    // Request handling
    internal async Task HandleRequest(HttpListenerRequest httpRequest, HttpListenerResponse httpResponse) {
      var routingResult = RouteRequest(httpRequest);
      var response = new ServerKit.HttpResponse(httpResponse);

      // No matching route found
      if (routingResult == null) {
        response.Status(404);

        return;
      }

      var request = new ServerKit.HttpRequest(routingResult, httpRequest);

      // Send the request to the route
      await routingResult.route.HandleRequest(request, response);
    }
  }
}