using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;


namespace Manganese.ServerKit {
  /**
   * @brief A HTTP server that receives requests and passes them to a router.
   */
  public class HttpDriver : IDisposable {
    /// @name Constants
    /// @{
    /**
     * @brief The port that a HTTP server will listen on if none is specified.
     */
    public static readonly int DEFAULT_PORT = 80;
    /// @}

    /// @name Constructors
    /// @{
    /**
     * @brief Create a new HTTP driver with a router and bind it to the default port at the root path prefix.
     *
     * @param router  The router to handle requests.
     *
     * Example:
     *
     * ```csharp
     * var driver = new HttpDriver(router);
     * ```
     */
    public HttpDriver(HttpRouter router) : this(router, String.Format("http://*:{0}/", DEFAULT_PORT)) {}

    /**
     * @brief Create a new HTTP driver with a router and bind it to the given prefix.
     *
     * @param router  The router to handle requests.
     * @param prefix  The prefix specifying the protocol, address, port, and path to listen at.
     *
     * Example:
     *
     * ```csharp
     * var driver = new HttpDriver(router, "http://*:80/");
     * ```
     */
    public HttpDriver(HttpRouter router, string prefix) : this(router, new List<string>() { prefix }) {}

    /**
     * @brief Create a new HTTP driver with a router and bind it to the given prefixes.
     *
     * @param router  The router to handle requests.
     * @param prefixes  The prefixes, each specifying a protocol, address, port, and path to listen at.
     *
     * Example:
     *
     * ```csharp
     * var driver = new HttpDriver(router, new List<string>() {
     *   "http://*:80/",
     *   "https://*:432/"
     * });
     * ```
     */
    public HttpDriver(HttpRouter router, List<string> prefixes) {
      this.router = router;

      // Create HTTP listener
      this.listener = new HttpListener();

      foreach (var prefix in prefixes) {
        this.listener.Prefixes.Add(prefix);
      }

      this.listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
    }
    /// @}

    // Router
    private readonly HttpRouter router;

    // Request handling
    private async Task HandleRequest(HttpListenerRequest httpRequest, HttpListenerResponse httpResponse) {
      await this.router.HandleRequest(httpRequest, httpResponse);
    }

    // HTTP listener
    private HttpListener listener;

    private async void ListenerCallback(IAsyncResult result) {
      var context = listener.EndGetContext(result);
      var httpRequest = context.Request;

      using (var httpResponse = context.Response) {
        try {
          await this.router.HandleRequest(httpRequest, httpResponse);
        } catch(Exception error) {
          Debug.Log("An exception occurred while handling a request:");
          Debug.Log(error);
          httpResponse.StatusCode = 500;
        }

        httpResponse.Close();
      }

      listener.BeginGetContext(ListenerCallback, listener);
    }

    /// @name Lifecycle
    /// @{
    /**
     * @brief Start the HTTP server.
     *
     * Example:
     *
     * ```csharp
     * var driver = new HttpDriver(router);
     * driver.Start();
     * ```
     */
    public void Start() {
      listener.Start();
      listener.BeginGetContext(ListenerCallback, null);
    }

    /**
     * @brief Stop the HTTP server, but allowing it to be started again in the future.
     *
     * Example:
     *
     * ```csharp
     * var driver = new HttpDriver(router);
     * driver.Start();
     * driver.Stop();
     * driver.Start(); // This is still possible
     * ```
     */
    public void Stop() {
      listener.Stop();
    }

    /**
     * @brief Stop the HTTP server permanently.
     *
     * Example:
     *
     * ```csharp
     * var driver = new HttpDriver(router);
     * driver.Start();
     * driver.Close();
     * ```
     */
    public void Close() {
      listener.Close();
    }
    /// @}

    /// @name Disposable
    /**
     * @brief Dispose of the HTTP server by permanently closing it.
     *
     * @remark Useful for short-lived servers in tests.
     *
     * Example:
     *
     * ```csharp
     * using (var driver = new HttpDriver(router)) {
     *   driver.Start();
     *   ...
     * }
     *
     * // Equivalent to:
     * var driver = new HttpDriver(router);
     * driver.Start();
     * ...
     * driver.Close();
     * ```
     */
    /// @{
    public void Dispose() {
      Close();
    }
    /// @}
  }
}